#
# Copyright © 2024 Emil Johansen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
# associated documentation files (the “Software”), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial
# portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
# LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
# NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
{ lib, pkgs, config, ... }:
let
  cfg = config.services.cloudflare.relay;

  destination.options = with lib; {
    host = mkOption {
      type = types.str;
      description = "Internal DNS";
      default = "localhost";
    };
    port = mkOption {
      type = types.ints.u16;
      description = "Port being relayed";
      default = 80;
    };
  };
in
with lib;
{
  options.services.cloudflare.relay = {
    enable = mkEnableOption "Cloudflare relay";
    tunnel = mkOption {
      type = types.str;
      description = "Tunnel ID from `cloudflared tunnel list` or web interface - " +
        "ex 6ff42ae2-765d-4adf-8112-31c55c1551ef";
    };
    credentialsFile = mkOption {
      type = types.str;
      description = "Path to json file readable by `services.cloudflared.user` " +
        "or `group`, holding credentials generated via `cloudflared tunnel create`";
    };
    # NOTE: This does not actually work. Unlike our own relay, cloudflare seems to only support HTTP.
    # The support for TCP attempted here requires a relay client on the other end as well.
#    dns.tcp = mkOption {
#      type = with types; attrsOf (submodule destination);
#      description = "DNS-TCP pairs";
#      default = {};
#    };
    dns.http = mkOption {
      type = with types; attrsOf (submodule destination);
      description = "DNS-HTTP pairs";
      default = {};
    };
    default = mkOption {
      type = types.str;
      description = "Relay response for unmapped traffic";
      default = "http_status:404";
    };
  };

  config = mkIf cfg.enable {
    services.cloudflared = {
      enable = true;
      tunnels = {
        "${cfg.tunnel}" = {
          credentialsFile = cfg.credentialsFile;
          default = cfg.default;
          ingress =
            #builtins.mapAttrs (n: v: "tcp://${v.host}:${toString v.port}") cfg.dns.tcp //
            builtins.mapAttrs (n: v: "http://${v.host}:${toString v.port}") cfg.dns.http;
        };
      };
    };
  };
}
