#
# Copyright © 2023 Emil Johansen
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
# associated documentation files (the “Software”), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial
# portions of the Software.
# 
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
# LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
# NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
{ lib, pkgs, config, ... }:
let
  relayedPorts.options = with lib; {
    local = mkOption {
      type = types.port;
      description = "Local machine port to be forwarded to";
    };
    remote = mkOption {
      type = types.port;
      description = "Relay port to forward from";
    };
  };
  
  relayNamePrefix = "relay-";
  keepAliveSeconds = 60;
  keepAliveRetry = 10;
  serviceRestartDelaySeconds = 60;
  
  relayScript = name: config: (pkgs.writeShellApplication {
    name = "${relayNamePrefix}${name}";
    runtimeInputs = [ pkgs.openssh ];
    text = ''
      ssh -R ${toString config.remote}:localhost:${toString config.local} \
        ${cfg.host.user}@${cfg.host.address} \
        -p ${toString cfg.host.port} \
        -Nn \
        -o ExitOnForwardFailure=yes \
        -o ServerAliveInterval=${toString keepAliveSeconds} \
        -o ServerAliveCountMax=${toString keepAliveRetry}
    '';
  }); 

  cfg = config.services.relayed;
in
with lib;
{
  options.services.relayed = {
    enable = mkEnableOption "Forward TCP traffic via SSH from port on remote relay to local port";
    host = {
      address = mkOption {
        type = types.str;
        description = "IP or DNS of the relay";
      };
      port = mkOption {
        type = types.port;
        description = "SSH port for the relay";
      };
      user = mkOption {
        type = types.str;
        description = "SSH user on the relay";
      };
      key-paths = {
        identity = mkOption {
          type = types.str;
          description = "Path to file containing the public SSH key used to identify the relay";
        };
        authentication = mkOption {
          type = types.str;
          description = "Path to file containing the private SSH key used to authenticate with the relay";
        };
      };
    };
    ports = mkOption {
      type = with types; attrsOf (submodule relayedPorts);
      description = "Ports to relay";
      default = {};
    };
  };

  config = mkIf cfg.enable
  {
    programs.ssh = {
      knownHosts.cloud = {
        hostNames = [ cfg.host.address ];
        publicKeyFile = cfg.host.key-paths.identity;
      };
      extraConfig = ''
        Host ${cfg.host.address}
        IdentitiesOnly yes
        IdentityFile ${cfg.host.key-paths.authentication}
      '';
    };

    systemd.services = mapAttrs' (
      name: ports: nameValuePair
        ("${relayNamePrefix}${name}")
        ({
          serviceConfig = {
            Type = "simple";
            ExecStart = (relayScript name ports) + "/bin/${relayNamePrefix}${name}";
            Restart = "always";
            RestartSec = serviceRestartDelaySeconds;
          };
          wantedBy = [ "multi-user.target" ];
          after = [ "network-online.target" ];
        })
    ) cfg.ports;
  };
}
