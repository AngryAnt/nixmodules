#
# Copyright © 2023 Emil Johansen
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
# associated documentation files (the “Software”), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial
# portions of the Software.
# 
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
# LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
# NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
{ lib, pkgs, config, ... }:
let
  features = rec {
    nixos = cfg.enable && cfg.nixos.enable;
    git = nixos && cfg.nixos.gitRebuild.enable;
    auto-git = git && cfg.nixos.gitRebuild.auto.enable;
    flatpak = cfg.enable && cfg.flatpak.enable;
  };

  scripts.nixos-rebuild-git = with pkgs; writeShellApplication {
    name = "nixos-rebuild-git";
    runtimeInputs = [
      git
      openssh
      nixos-rebuild
      sudo
    ];
    text = ''
      cd "${cfg.nixos.gitRebuild.repository.path}"

      remote=${cfg.nixos.gitRebuild.repository.remote}
      branch=${cfg.nixos.gitRebuild.repository.branch}

      same_commit () {
        readarray -t < <(git show-ref --hash --verify "$@") hashes
        [ "''${hashes[0]}" = "''${hashes[1]}" ]
      }

      git fetch "$remote"
      if same_commit "refs/heads/$branch" "refs/remotes/$remote/$branch"; then
        echo "Already up to date."
        exit
      fi

      git pull --rebase "$remote"
      git submodule sync --recursive
      git submodule deinit --all -f
      git submodule init
      git submodule update --recursive --force --checkout
      sudo nixos-rebuild "$@"
    '';
  };

  cfg = config.services.update;
in with lib;
{
  options.services.update = {
    enable = mkEnableOption "System auto-update services";
    nixos = {
      enable = mkOption {
        type = types.bool;
        default = true;
        description = "Update the nixos system";
      };
      attended = mkOption {
        type = types.bool;
        default = true;
        description = "Assume that the system is attended - no automatic reboots";
      };
      gitRebuild = {
        enable = mkOption {
          type = types.bool;
          default = false;
          description = "Monitor git remote for updates and run nixos-rebuild after pulling them";
        };
        auto = {
          enable = mkOption {
            type = types.bool;
            default = true;
            description = "Run nixos-rebuild-git intermittently in a service - set to false for manual-only";
          };
          intervalSeconds = mkOption {
            type = types.int;
            default = 60;
            description = "Delay between each run";
          };
          command = mkOption {
            type = types.str;
            default = "switch";
            description = "Command string passed to nixos-rebuild";
          };
        };
        repository = {
          path = mkOption {
            type = types.str;
            default = "/home/$(whoami)/.nixos/host-$(hostname)";
            description = "Local path to the repository clone";
          };
          branch = mkOption {
            type = types.str;
            default = "trunk";
            description = "Branch to monitor on remote and pull locally";
          };
          remote = mkOption {
            type = types.str;
            default = "origin";
            description = "Name of the remote to monitor";
          };
        };
      };
    };
    flatpak = {
      enable = mkOption {
        type = types.bool;
        description = "Update system-installed flatpaks";
        default = config.services.flatpak.enable;
      };
      startDelaySeconds = mkOption {
        type = types.int;
        description = "Delay seconds from service start (network available) to update start";
        default = 10;
      };
    };
  };

  config = mkMerge [
    (mkIf features.nixos
    {
      system.autoUpgrade = {
        enable = true;
        operation = "boot";
        dates = "daily";
        randomizedDelaySec = "45min";
        allowReboot = !cfg.nixos.attended;
        rebootWindow = {
          lower = "03:00";
          upper = "05:00";
        };
      };

      nix.gc = {
        automatic = true;
        dates = "daily";
        randomizedDelaySec = "45min";
        options = "--delete-older-than 7d";
      };
    })

    (mkIf features.git
    {
      programs.git.enable = true;
      environment.systemPackages = [
        scripts.nixos-rebuild-git
      ];
    })

    (mkIf features.auto-git
    {
      systemd.services.auto-rebuild-git = {
        serviceConfig = {
          Type = "simple";
          User = "root";
          Restart = "always";
          RestartSec = cfg.nixos.gitRebuild.auto.intervalSeconds;
        };
        path = [ scripts.nixos-rebuild-git ];
        script = ''nixos-rebuild-git ${cfg.nixos.gitRebuild.auto.command}'';
        environment.NIX_PATH =
          "nixpkgs=/nix/var/nix/profiles/per-user/root/channels/nixos:" +
          "nixos-config=/etc/nixos/configuration.nix:" +
          "/nix/var/nix/profiles/per-user/root/channels";
      };
    })

    (mkIf features.flatpak
    {
      systemd.services.flatpak-auto-update = {
        serviceConfig = {
          Type = "oneshot";
          User = "root";
        };
        wantedBy = [ "multi-user.target" ];
        after = [ "network-online.target" ];
        path = [ pkgs.flatpak ];
        script = ''
          sleep ${toString cfg.flatpak.startDelaySeconds}
          flatpak update --assumeyes
        '';
      };
    })
  ];
}
