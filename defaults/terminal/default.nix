#
# Copyright © 2023 Emil Johansen
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
# associated documentation files (the “Software”), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial
# portions of the Software.
# 
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
# LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
# NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
# TODO: Generate zsh and p10k config files for all users
{ lib, pkgs, config, ... }:
let
  cfg = config.programs.preferred-terminal;

  scripts = {
    plasma.logout = pkgs.writeShellApplication {
      name = "plasma-logout";
      runtimeInputs = [ pkgs.libsForQt5.qt5.qttools ];
      text = ''
        qdbus org.kde.Shutdown /Shutdown logout
      '';
    };
    edit-create = pkgs.writeShellApplication {
      name = "edit-create";
      runtimeInputs = [ cfg.default-editor ];
      text = ''
        path=''${1:-""}
        if [ -z "$path" ]; then
          ${lib.getName cfg.default-editor}
        else
          if [ ! -f "$path" ]; then
            touch "$path"
          fi
          ${lib.getName cfg.default-editor} "$path"
        fi
      '';
    };
    prefix-rename = pkgs.writeShellApplication rec {
      name = "prefix-rename";
      text = ''
        if [ $# -ne 2 ]; then
          echo "Usage: ${name} <current-prefix> <desired-prefix>"
          exit
        fi
        
        read -r -n 1 -p "Rename '$1-something' files & directories to '$2-something' ones? [yN]" answer
        case ''${answer:0:1} in
          y|Y )
            echo
          ;;
          * )
            exit
          ;;
        esac
        
        for f in "$1"*; do
          if [ ! -e "$f" ]; then
            break
          fi
          newname="$2""''${f#"$1"}"
          mv --verbose --interactive "$f" "$newname"
        done
      '';
    };
  };
in
with lib;
{
  options.programs.preferred-terminal = {
    enable = mkEnableOption "Preferred terminal setup";
    default-editor = mkOption {
      type = types.package;
      description = "Package to use as default editor";
      default = pkgs.micro;
    };
    edit-command = mkOption {
      type = types.str;
      description = "Command to run with the 'edit' alias";
      default = getName scripts.edit-create;
    };
  };

  config = mkIf cfg.enable (mkMerge [
    ({
      programs.zsh = {
        enable = true;
        ohMyZsh.enable = true;
        promptInit = "source ${pkgs.zsh-powerlevel10k}/share/zsh-powerlevel10k/powerlevel10k.zsh-theme";
      };
  
      programs.thefuck.enable = true;
  
      environment = {
        systemPackages = with pkgs; [
          zsh-powerlevel10k
          tealdeer
          bat
          eza
          jq
          btop
          du-dust
          duf
          cfg.default-editor
          scripts.edit-create
          scripts.prefix-rename
        ];
        variables.EDITOR = getName cfg.default-editor;
        shellAliases =
        let
          lsAlias = "ls -alg";
        in {
          edit = cfg.edit-command;
          nano = ''function f(){ echo "Use edit in stead"; unset -f f; }; f''; # Killing old habits
          cat = ''bat --paging=never --decorations=never'';
          ls = ''eza'';
          l = ''ls'';
          la = lsAlias;
          lsa = lsAlias;
          ll = lsAlias;
          top = ''btop'';
          du = ''dust --no-percent-bars'';
          df = ''duf'';
          guid = ''uuidgen'';
          mac = ''(printf '%02x' $((0x$(od /dev/urandom -N1 -t x1 -An | tr -d ' ') & 0xFE | 0x02)); od /dev/urandom -N5 -t x1 -An | tr ' '  ':')'';
          chop = ''truncate -s-1'';
          nixos-rebuild = ''nixos-rebuild --use-remote-sudo'';
          nix-search = ''nix --extra-experimental-features nix-command --extra-experimental-features flakes search nixpkgs'';
        };
      };

      users.defaultUserShell = pkgs.zsh;
    })
    
    (mkIf (
      config.services.xserver.desktopManager.plasma5.enable ||
      config.services.xserver.desktopManager.plasma5.mobile.enable ||
      config.services.xserver.desktopManager.plasma5.bigscreen.enable
    ) {
      environment.systemPackages = [
        scripts.plasma.logout
      ];
    })
  ]);
}
