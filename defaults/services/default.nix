#
# Copyright © 2023 Emil Johansen
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
# associated documentation files (the “Software”), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial
# portions of the Software.
# 
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
# LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
# NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
{ lib, pkgs, config, ... }:
let
  enableSSH = cfg.enable && cfg.user.ssh.login-key-files != [];

  cfg = config.defaults.service.base;
in
with lib;
{
  imports = [
    ../terminal
    ../../update
  ];

  options.defaults.service.base = {
    enable = mkEnableOption "Base configuration defaults for service system";
    host-name = mkOption {
      type = types.str;
      description = "Network host name to use with DHCP network config";
    };
    network-interface = mkOption {
      type = types.str;
      default = "enp6s18";
      description = "Name of the main network interface to configure";
    };
    user = {
      name = mkOption {
        type = types.str;
        description = "Name to use for the default user";
      };
      ssh.login-key-files = mkOption {
        type = types.listOf (types.path);
        description = "Public SSH keys able to log in as the default user (without keys SSH is not enabled)";
      };
    };
  };

  config = mkMerge [
    (mkIf cfg.enable
    {    
      boot = {
        loader = {
          systemd-boot.enable = true;
          efi.canTouchEfiVariables = true;
        };
        kernelParams = [
          "console=ttyS0,115200"
          "console=tty1"
        ];
        tmp.useTmpfs = true;
      };
  
      fileSystems."/packages".neededForBoot = true;
      services.qemuGuest.enable = true;
      zramSwap.enable = true;
      services.fstrim.enable = true;
      nix.settings.auto-optimise-store = true;
  
      networking = {
        hostName = cfg.host-name;
        useDHCP = false;
        interfaces."${cfg.network-interface}".useDHCP = true;
      };
    
      time.timeZone = "Europe/Copenhagen";
      i18n.defaultLocale = "en_US.UTF-8";
      console = {
        font = "Lat2-Terminus16";
        keyMap = "us";
      };
    
      programs.preferred-terminal.enable = true;
    
      users.users."${cfg.user.name}" = {
        isNormalUser = true;
        extraGroups = [ "wheel" ];
      };
  
      nixpkgs.config.allowUnfree = true;    
      environment.systemPackages = with pkgs; [
        parted
        xfsprogs
      ];
  
      documentation.doc.enable = false;

      services.update = {
        enable = true;
        nixos = {
          attended = false;
          gitRebuild = {
            enable = true;
            auto.enable = lib.mkDefault false;
            repository.path = lib.mkDefault "/home/${cfg.user.name}/.nixos/host-${cfg.host-name}";
          };
        };
      };
  
      system.copySystemConfiguration = true;
    })

    (mkIf enableSSH {
      services.openssh = {
        enable = true;
        startWhenNeeded = true;
        openFirewall = true;
        settings = {
          PermitRootLogin = "no";
          PasswordAuthentication = false;
          KbdInteractiveAuthentication = false;
        };
      };
      
      users.users."${cfg.user.name}".openssh.authorizedKeys.keyFiles = cfg.user.ssh.login-key-files;
    })
  ];
}
